package com.speechtotext

interface FetcherListener {
    fun doneFetching()
    fun beginFetching()
}