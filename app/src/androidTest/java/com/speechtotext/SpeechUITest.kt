package com.speechtotext

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.speechtotext.ui.activity.MainActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class SpeechUITest {

    @get:Rule
    val mActivityRule = ActivityTestRule(MainActivity::class.java, false, true)

    @Test
    fun ensureSpeechToText() {
        ensureStartListening()
//        onView(withId(R.id.auto_tv_search_box))
//            .check(matches(withText("hello")))
    }

    private fun ensureStartListening() {
        onView(withId(R.id.btn_mic))
            .perform(click())
    }
}