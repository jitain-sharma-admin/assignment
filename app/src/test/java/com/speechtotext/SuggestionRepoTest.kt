package com.speechtotext

import com.speechtotext.model.db.dao.QuesDao
import com.speechtotext.model.repo.SuggestionRepo
import com.speechtotext.model.service.SearchService
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.hasItems
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.FixMethodOrder
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.MethodSorters
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
class SuggestionRepoTest {

//    @ExperimentalCoroutinesApi
//    private val testDispatcher = TestCoroutineDispatcher()

    private lateinit var suggestionRepo: SuggestionRepo

    @Before
    fun init() {
        val quesDaoMock = mock(QuesDao::class.java)
        val searchService = SearchService()
        suggestionRepo = SuggestionRepo(searchService, quesDaoMock)
    }

    @Test
    @Throws(Exception::class)
    fun whenAssertingSuggestions_thenVerified() {

        val word = "Who is the Microsoft CEO?"
        val word2 = "Who is the Apple CEO?"
        val word3 = "Who is Tim Cook?"
        val word4 = "Who is the Prime Minster of India?"

        runBlocking { suggestionRepo.saveSuggestions(word) }
        runBlocking { suggestionRepo.saveSuggestions(word2) }
        runBlocking { suggestionRepo.saveSuggestions(word3) }
        runBlocking { suggestionRepo.saveSuggestions(word4) }

        val result = runBlocking { suggestionRepo.querySuggestions("Who is") }

        assertThat(result, hasItems(word, word2, word3, word4))
    }

    @Test
    @Throws(Exception::class)
    fun whenSuggestionsNotFound_thenVerified() {

        val result = runBlocking { suggestionRepo.querySuggestions("Best book to read") }

        assert(true) { result.isEmpty() }
    }

}