package com.speechtotext.ui.utils.extensions

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.observe

inline fun <T> LiveData<T>.lifeCycleAwareObserver(lifecycleOwner: LifecycleOwner, crossinline work: (T) -> Unit) {
    this.observe(lifecycleOwner) {
        if (lifecycleOwner.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED))
            work(it)
    }
}