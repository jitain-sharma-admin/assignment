package com.speechtotext.ui.activity

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ArrayAdapter
import androidx.core.app.ActivityCompat
import com.speechtotext.BR
import com.speechtotext.R
import com.speechtotext.databinding.SearchActivityBinding
import com.speechtotext.model.service.device.SpeechService
import com.speechtotext.ui.utils.extensions.lifeCycleAwareObserver
import com.speechtotext.viewmodel.SearchViewModel


class MainActivity : BaseActivity<SearchActivityBinding, SearchViewModel>() {

    override val layoutRes: Int = R.layout.search_activity

    override fun viewModel() = SearchViewModel::class.java

    override fun bindingVariable() = BR.viewModel

    private val permissions = arrayOf(Manifest.permission.RECORD_AUDIO)
    private val REQUEST_RECORD_AUDIO_PERMISSION = 65

    private lateinit var adapter: ArrayAdapter<String>

    private val handlerRipple: Handler = Handler()
    private var isRippleRunning = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = ArrayAdapter<String>(
            this,
            android.R.layout.simple_dropdown_item_1line, arrayOf()
        )

        dataBinding.autoTvSearchBox.setAdapter(adapter)

        dataBinding.btnMic.setOnClickListener(micClickListener)
        setupSpeechViewModel()
    }

    override fun onPause() {
        stopRippleAnim()
        viewModel.stopListening()
        super.onPause()
    }

    private val micClickListener = View.OnClickListener {
        if (!viewModel.permissionToRecordAudio) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION)
            return@OnClickListener
        }

        if (viewModel.isListening)
            viewModel.stopListening()
        else {
            dataBinding.autoTvSearchBox.requestFocus()
            viewModel.startListening()
        }
    }

    private fun setupSpeechViewModel() {
        viewModel.getViewState().lifeCycleAwareObserver(this) { render(it) }
        viewModel.onSuggestionsLiveData.lifeCycleAwareObserver(this) { listOfSuggestions ->
            adapter.apply {
                clear()
                addAll(listOfSuggestions)
                notifyDataSetChanged()
            }
        }
    }

    private fun render(uiOutput: SpeechService.ViewState?) {

        uiOutput?.let {
            viewModel.suggestions(it.spokenText)

            if (!it.isPartial)
                viewModel.saveSuggestions(it.spokenText)

            dataBinding.autoTvSearchBox.setText(it.spokenText)

            if (it.isListening) {
                if (!isRippleRunning)
                    startRippleAnim()
            } else
                stopRippleAnim()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_RECORD_AUDIO_PERMISSION)
            viewModel.permissionToRecordAudio = grantResults[0] == PackageManager.PERMISSION_GRANTED

        if (viewModel.permissionToRecordAudio)
            dataBinding.btnMic.performClick()
    }

    private fun startRippleAnim() {
        isRippleRunning = true
        handlerRipple.postDelayed(rippleRunnable, 400)
    }

    private fun stopRippleAnim() {
        handlerRipple.removeCallbacks(rippleRunnable)
        isRippleRunning = false
    }

    private val rippleRunnable = Runnable {
        dataBinding.btnMic.animate().scaleX(2f).scaleY(2f).alpha(0.5f).setDuration(300)
            .withEndAction {
                dataBinding.btnMic.scaleX = 1f
                dataBinding.btnMic.scaleY = 1f
                dataBinding.btnMic.alpha = 1f
            }
        startRippleAnim()
    }
}