package com.speechtotext.di.module

import com.speechtotext.di.scopes.ActivityScope
import com.speechtotext.ui.activity.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [])
    abstract fun contributeMainActivity(): MainActivity

}