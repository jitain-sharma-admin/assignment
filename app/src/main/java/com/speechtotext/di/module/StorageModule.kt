package com.speechtotext.di.module

import android.app.Application
import androidx.room.Room
import com.commonsware.cwac.saferoom.SafeHelperFactory
import com.speechtotext.model.db.AppDb
import com.speechtotext.model.db.dao.QuesDao
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
object StorageModule {

    @Provides
    @Reusable
    @JvmStatic
    fun provideSafeHelperFactory(): SafeHelperFactory {
        return SafeHelperFactory("zbk740vwhs73pagl4628nqo".toCharArray())
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideAppDb(application: Application, safeHelperFactory: SafeHelperFactory): AppDb {
        return Room.databaseBuilder(application.applicationContext, AppDb::class.java, "speech.db")
            .fallbackToDestructiveMigration()
            .openHelperFactory(safeHelperFactory)
            .build()
    }

    @Provides
    @Singleton
    @JvmStatic
    fun provideQuesDao(db: AppDb): QuesDao = db.quesDao()

}