package com.speechtotext.di.module

import com.speechtotext.model.service.SearchService
import dagger.Module
import dagger.Provides

@Module
class RepoModule {

    @Provides
    fun provideSearchService(): SearchService {
        return SearchService()
    }

}