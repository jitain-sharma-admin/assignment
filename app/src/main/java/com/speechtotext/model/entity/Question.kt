package com.speechtotext.model.entity

import androidx.room.Entity
import androidx.room.Index

@Entity(
    indices = [Index(value = ["queryString"], unique = true)],
    primaryKeys = ["queryString"]
)
data class Question(

    val queryString: String
)