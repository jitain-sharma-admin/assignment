package com.speechtotext.model.entity;

public class TrieNode {
    public TrieNode[] child;
    private char label;
    public boolean isLast;
    public String word;
    private static final int ALPHABET_SIZE = 26;

    private TrieNode() {
        this.child = new TrieNode[ALPHABET_SIZE];
    }

    public TrieNode(char l) {
        this();
        this.label = l;
    }

    public boolean isEmpty() {
        return label == ' ';
    }
}
