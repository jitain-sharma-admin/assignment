package com.speechtotext.model.service;

import android.text.TextUtils;

import com.speechtotext.model.entity.TrieNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class SearchService {
    private TrieNode root;
    private final char startChar = 'A';

    public SearchService() {
        this.root = new TrieNode(' ');
    }

    public boolean isEmpty() {
        return root.isEmpty();
    }

    public void insertNewWord(String word) {
        char[] cArr = word.toUpperCase().toCharArray();
        TrieNode node = root;
        TrieNode tn;
        int index = 0;
        do {
            //65-90 Char value for A-Z
            while (cArr[index] < 65 || cArr[index] > 90)
                index++;

            tn = node.child[cArr[index] - startChar];
            if (tn != null) {
                node = tn;
                index++;
                if (index >= word.length()) {
                    node.isLast = true;
                    node.word = word;
                    return;
                }
            }
        } while (tn != null);

        for (; index < cArr.length; index++) {
            if (cArr[index] >= 65 && cArr[index] <= 90) {
                node.child[cArr[index] - startChar] = new TrieNode(cArr[index]);
                node = node.child[cArr[index] - startChar];
            }
        }

        node.isLast = true;
        node.word = word;
    }

    public List<String> getWordsByPrefix(String word) {

        List<String> words = new ArrayList<>();
        if (TextUtils.isEmpty(word)) return words;

        char[] cArr = word.toUpperCase().toCharArray();
        TrieNode node = root;
        TrieNode tn;
        int index = 0;
        int value = 0;
        do {
            //65-90 Char value for A-Z
            if (cArr[index] >= 65 && cArr[index] <= 90) {
                value = cArr[index] - startChar;
                tn = node.child[value];

                if (tn == null) return words;
                node = tn;
            }
            index++;

        } while (index < cArr.length);

        Deque<TrieNode> DQ = new ArrayDeque<>();
        DQ.addLast(node);
        while (!DQ.isEmpty()) {
            TrieNode first = DQ.removeFirst();
            if (first.isLast) {
                words.add(first.word);
            }
            for (TrieNode n : first.child) {
                if (n != null) {
                    DQ.add(n);
                }
            }
        }
        return words;
    }
}
