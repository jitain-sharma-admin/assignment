package com.speechtotext.model.repo

import com.speechtotext.model.db.dao.QuesDao
import com.speechtotext.model.entity.Question
import com.speechtotext.model.service.SearchService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SuggestionRepo @Inject constructor(
    private val searchService: SearchService,
    private val quesDao: QuesDao
) {

    suspend fun loadSuggestions() =
        withContext(Dispatchers.IO) {
            //Load from DB
            //TODO: the data maybe huge, apply the count for most used in DB and load top 50 or some custom only
            val dbList: List<String>? = withContext(Dispatchers.IO) { quesDao.getAllQues() }
            //Load to trie data structure
            dbList?.let { it.forEach { w -> searchService.insertNewWord(w) } }
        }

    suspend fun saveSuggestions(word: String) =
        withContext(Dispatchers.Default) {
            if (word.isNotEmpty() && word.contains(" ")) {
                //Also save in the database
                withContext(Dispatchers.IO) { quesDao.insert(Question(word)) }
                searchService.insertNewWord(word)
            }
        }

    suspend fun querySuggestions(word: String): List<String> =
        withContext(Dispatchers.Default) {
            //Search it in the trie
            searchService.getWordsByPrefix(word)
        }

}