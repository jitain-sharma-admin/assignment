package com.speechtotext.model.db.dao

import androidx.room.Dao
import androidx.room.Query
import com.speechtotext.model.entity.Question

@Dao
abstract class QuesDao : BaseDao<Question> {

    @Query("SELECT * FROM Question")
    abstract suspend fun getAllQues(): List<String>

}