package com.speechtotext.model.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.speechtotext.model.db.dao.QuesDao
import com.speechtotext.model.entity.Question

@Database(
    entities = [Question::class],
    version = 1,
    exportSchema = false
)
abstract class AppDb : RoomDatabase() {
    abstract fun quesDao(): QuesDao
}