package com.speechtotext.viewmodel

import android.Manifest
import android.app.Application
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import com.speechtotext.model.repo.SuggestionRepo
import com.speechtotext.model.service.device.SpeechService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    application: Application,
    private val speechService: SpeechService,
    private val suggestionRepo: SuggestionRepo
) : AndroidViewModel(application) {

    init {
        viewModelScope.launch {
            suggestionRepo.loadSuggestions()
        }
    }

    private val _onSuggestionsAvailMLiveData: MutableLiveData<String> = MutableLiveData()

    var isListening = false
        get() = speechService.isListening

    var permissionToRecordAudio = checkAudioRecordingPermission(context = application)

    private fun checkAudioRecordingPermission(context: Application) =
        ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED

    fun getViewState(): LiveData<SpeechService.ViewState> = speechService.getViewState()

    fun startListening() = speechService.startListening()
    fun stopListening() = speechService.stopListening()

    fun suggestions(word: String?) {
        word?.takeIf { it.isNotEmpty() }?.apply { _onSuggestionsAvailMLiveData.postValue(this) }
    }

    fun saveSuggestions(word: String?) {
        word?.takeIf { it.isNotEmpty() }?.apply {
            viewModelScope.launch {
                suggestionRepo.saveSuggestions(this@apply)
            }
        }
    }

    val onSuggestionsLiveData: LiveData<List<String>> =
        _onSuggestionsAvailMLiveData.switchMap { word ->
            liveData {
                val data = MutableLiveData<List<String>>()
                emitSource(
                    withContext(Dispatchers.Main) {
                        print("onSuggestionsLiveData entered")
                        if (word.isNotEmpty())
                            data.value = suggestionRepo.querySuggestions(word)
                        print("onSuggestionsLiveData completed")
                        data
                    }
                )
            }
        }
}